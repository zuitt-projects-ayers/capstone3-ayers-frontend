// // import { useEffect, useState } from 'react';
// // import { Row, Col } from 'react-bootstrap';


// // export default function CartView(orderProp){

// // // 	const {}

// // // 	const [orders, setOrders] = useState([]);

// // // 	useEffect(() => {
// // // 		fetch('http://localhost:4000/getOrders')
// // // 		.then(res => res.json())
// // // 		.then(data => {
// // // 			console.log(data)

// // // 			setOrders(data.map(order => {
// // // 				return (
// // // 					<tr>
// // // 						<td></td>

// // // 					</tr>
// // // 				)
// // // 			}))

// // // 		})
// // // 	})

// // 	return(
// // 		<>
// // 			<h1 className="my-3 text-center">Your Cart</h1>
// // 				<table className="mx-auto">
// // 					<tr className="bg-dark text-white">
// // 						<th className="px-5">Name</th>
// // 						<th className="px-5">Price</th>
// // 						<th className="px-5">Quantity</th>
// // 						<th className="px-5">Subtotal</th>
// // 					</tr>
					
// // 				</table>
// // 		</>
// // 	)
// // }


import { Fragment, useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CartView(props){

	const navigate = useNavigate()

	const { orderData, fetchData } = props

	console.log("orderData")
	console.log(orderData)

	const [orders, setOrders] = useState([]);
	const [total, setTotal] = useState(0)


	const cancelOrder = (orderId, orderStatus) => {

		console.log(orderStatus);

		fetch(`https://immense-badlands-93637.herokuapp.com/users/${ orderId }/cancel`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				orderStatus: "Cancelled"
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				console.log(data)

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Order successfully cancelled."
				});

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}
		})
	}

	const purchaseOrder = (orderId) => {

		fetch(`https://immense-badlands-93637.herokuapp.com/users/${ orderId }/purchase`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				orderStatus: "Purchased"
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				console.log(data)

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Order(s) successfully purchased."
				});

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}
		})
	}

	const purchaseAllOrders = (orderData) => {
		orderData.map(order => purchaseOrder(order._id))
	}

	useEffect(() => {

		let totalCarrier = 0
		let subtotal = 0

		const ordersArr = orderData.map(order => {

			console.log(order.totalAmount)
			console.log(order.productPrice)
			totalCarrier += (order.totalAmount * order.productPrice)
			subtotal = (order.totalAmount * order.productPrice)
			console.log(totalCarrier)
			console.log("stop")


			return(

				<tr key={order._id}>
					<td>{order.productName}</td>
					<td>PhP {order.productPrice}</td>
					<td className="text-center">{order.totalAmount}</td>
					<td>PhP {subtotal.toFixed(2)}</td>
					<td>
						<Button 
							className="w-100"
							variant="danger" 
							size="sm" 
							onClick={() => cancelOrder(order._id, order.orderStatus)}
						>
							Remove
						</Button>
					</td>
				</tr>
			)
		})

		setOrders(ordersArr)
		setTotal(totalCarrier)

	}, [orderData, fetchData])

	return(
		<Fragment>
			<h1 className=" text-center mb-3">Cart</h1>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th className="text-center">Name</th>
						<th className="text-center">Price</th>
						<th className="text-center">Quantity</th>
						<th className="text-center">Subtotal</th>
						<th></th>
					</tr>					
				</thead>
				<tbody>
					{orders}
					<tr>
						<th colSpan="3">
							{ (orderData.length === 0) ?
							<Button variant="success" className="w-100" onClick={() => {
									navigate("/")
								}}>Back to Store</Button>

								: 

								<Button variant="success" className="w-100" onClick={() => {
									purchaseAllOrders(orderData)
									navigate("/")
								}}>Checkout</Button>
							}
						</th>
						<th colSpan="2" className="text-center">Total: {total.toFixed(2)}</th>
					</tr>
				</tbody>
	
			</Table>
		</Fragment>
	)
}