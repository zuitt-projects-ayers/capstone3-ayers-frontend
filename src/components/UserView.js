import React, {useState, useEffect} from 'react'

import { Col } from 'react-bootstrap'

import ProductCard from '../components/ProductCard';

export default function UserView({productData}){

	//console.log("productData")
	// console.log(productData)

	const [product, setProducts] = useState([])

	useEffect(() => {
		fetch('https://immense-badlands-93637.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setProducts(data.map(product => {
				return (
					<Col xs={12} md={6} lg={4} className="my-3" key={product._id}>
						<ProductCard productProp={product}/>
					</Col>
				);
			}));
		})
	}, [])

	return (
		<>
			{product}
		</>
	)
}