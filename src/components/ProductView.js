import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [releaseDate, setReleaseDate] = useState('');

	const [totalAmount, setTotalAmount] = useState(1)

	const {productId} = useParams();

	const order = (productId) => {

		console.log('HERE')
		console.log(productId)
		console.log(user.id)
		console.log(totalAmount)
		console.log(name)
		console.log(price)

		fetch('https://immense-badlands-93637.herokuapp.com/users/order', {
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body : JSON.stringify({
				userId : user.id,
				productId : productId,
				totalAmount : totalAmount,
				productName : name,
				productPrice : price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

				Swal.fire({
					title : "Ordered Successfully!",
					icon : "success",
					text : "You have successfully ordered this item."
				})

				navigate("/");

			} else {

				Swal.fire({
					title : "Something went wrong.",
					icon : "error",
					text : "Please contact admin."
				})

			}
		})
	}

	useEffect(() => {
		// console.log(productId)
		// console.log(user.id)
		// console.log(totalAmount)

		fetch(`https://immense-badlands-93637.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setReleaseDate(data.releaseDate);
		});
	}, [productId, totalAmount])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="productCard py-0 my-2 mx-3">
						<Card.Body className="pb-0">
							<Card.Title className="text-center mx-3">{name}</Card.Title>
							<Card.Text className="text-center mx-3">{description}</Card.Text>
							<Card.Subtitle className="text-center mx-3">Release Date:</Card.Subtitle>
							<Card.Text className="text-center mx-3">{releaseDate}</Card.Text>
							<Card.Subtitle className="text-center mx-3">Price:</Card.Subtitle>
							<Card.Text className="text-center mx-3">PhP {price}</Card.Text>
						</Card.Body>
						<Card.Footer>
							{ user.id !== null ?
								<>
									<Card.Subtitle className="text-center mb-2">Quantity:</Card.Subtitle>
									<Card.Text>
											<Form.Control className="text-center"
												type = "number"
												value = {totalAmount}
												onChange = {e => setTotalAmount(e.target.value)}
												required
											/>
									</Card.Text>
									<Button className="w-100" variant="primary" onClick={() => order(productId)}>Purchase Now!</Button>
								</>
								:
								<Link className="btn btn-danger w-100" to="/login">Login to Purchase</Link>
							}
						</Card.Footer>
					</Card>
				</Col>
			</Row>
		</Container>
	);
};

// <Form.Label>Email:</Form.Label>
// <Form.Control
// 	type = "email"
// 	placeholder = "Enter your email."
// 	value = {email}
// 	onChange = {e => setEmail(e.target.value)}
// 	required
// />
