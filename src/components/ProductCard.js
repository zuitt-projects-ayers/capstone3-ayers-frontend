import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}){

	const {name, description, price, releaseDate, _id} = productProp

	return(

		<Card className="productCard py-0 my-2 mx-3">
			<Card.Body className="pb-0">
{/*				<Card.Img variant="top" src="https://upload.wikimedia.org/wikipedia/en/1/15/The_Elder_Scrolls_V_Skyrim_cover.png" />
*/}				<Card.Title className="text-center mx-3">{name}</Card.Title>
				<Card.Text className="text-center mx-3">{description}</Card.Text>
				<Card.Subtitle className="text-center mx-3">Release Date:</Card.Subtitle>
				<Card.Text className="text-center mx-3">{releaseDate}</Card.Text>
				<Card.Subtitle className="text-center mx-3">Price:</Card.Subtitle>
				<Card.Text className="text-center mx-3">PhP {price}</Card.Text>
			</Card.Body>
			<Card.Footer>
				<Button className="w-100" variant="primary" as={Link} to={`/products/${_id}`}>See Details</Button>
			</Card.Footer>

		</Card>
	)
};