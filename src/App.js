import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Products from './pages/Products';
import Cart from './pages/Cart';
import Error from './pages/Error';
// import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import OrderHistory from './pages/OrderHistory';
import Register from './pages/Register';
import './App.css';
import {UserProvider} from './UserContext';

export default function App() {

  const [user, setUser] = useState({
    id : null,
    isAdmin : null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch('https://immense-badlands-93637.herokuapp.com/users/details', {
      method : "POST",
      headers : {
        Authorization :  `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined"){

        setUser({
          id : data._id,
          isAdmin : data.isAdmin
        })

      } else {
        setUser({
          id : null,
          isAdmin : null
        })
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
      <Routes>
        {/*<Route exact path = "/" element = {<Home/>}/>*/}
        <Route exact path = "/" element = {<Products/>}/>
        <Route exact path = "/products/:productId" element = {<ProductView/>}/>
        <Route exact path = "/register" element = {<Register/>}/>
        <Route exact path = "/login" element = {<Login/>}/>
        <Route exact path = "/logout" element = {<Logout/>}/>
        <Route exact path = "/cart" element = {<Cart/>}/>
        <Route exact path = "/orderHistory" element = {<OrderHistory/>}/>
        <Route exact path="*" element={<Error/>} />
      </Routes>
      </Container>
    </Router>
    </UserProvider>
  )
};