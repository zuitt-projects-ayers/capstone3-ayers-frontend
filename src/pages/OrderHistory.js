import React, { useState, useEffect, useContext } from 'react';
/*react-bootstrap component*/
import { Container, Row } from 'react-bootstrap'

/*components*/
// import Product from './../components/Product';
import CartView from './../components/OrderHistoryView.js';

/*context*/
import UserContext from './../UserContext';

export default function OrderHistory(){

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://immense-badlands-93637.herokuapp.com/users/getPurchasedOrders',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log("result")
			console.log(result)
			setOrders(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	// let CourseCards = courses.map( (course) => {
	// 	return <Course key={course.id} course={course}/>
	// })
 
	return(
		<Container className="p-4">
				<CartView orderData={orders} fetchData={fetchData}/>
		</Container>
	)
}