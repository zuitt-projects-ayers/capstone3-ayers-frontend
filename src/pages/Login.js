import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){

	const navigate = useNavigate()

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function authenticate(e){
		e.preventDefault();

		fetch('https://immense-badlands-93637.herokuapp.com/users/login', {
			method : "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title : 'Login Successful.',
					icon : 'success',
					text : 'Welcome back!'
				})

				navigate('/');

			} else {

				Swal.fire({
					title : 'Authentication Failed.',
					icon : 'error',
					text : 'Please check your credentials.'
				})
			}
		});

		setEmail('');
		setPassword('');

		const retrieveUserDetails = (token) => {
			fetch('https://immense-badlands-93637.herokuapp.com/users/details', {
				method : "POST",
				headers : {
					Authorization : `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setUser({
					id : data._id,
					isAdmin : data.isAdmin
				});
			});
		};
	};

	useEffect(() => {

		if (email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		};

	}, [email, password])

	return(

		(user.id !== null) ?

		<Navigate to="/products"/>

		:

		<>
		<Form onSubmit={e => authenticate(e)}>
			<h1 className="my-3 text-center">Login</h1>
			<Container  className="border border-2 rounded-top">
				<Form.Group controlId="email" className="my-3">
					<Form.Label>Email:</Form.Label>
					<Form.Control
						type = "email"
						placeholder = "Enter your email."
						value = {email}
						onChange = {e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password" className="my-3">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Enter your password."
						value = {password}
						onChange = {e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>
			</Container>

			<Container  className="border border-top-0 border-2 rounded-bottom">
				{ isActive ?
					<Button variant="success" type="submit" id="submitBtn" className="my-3 w-100">
						Login
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" className="my-3 w-100" disabled>
						Please enter your account details.
					</Button>
				}
			</Container>
		</Form>

		<p className="text-center my-3">
			Don't have an account yet? <Link to="/register" className="text-decoration-none">Click here</Link> to register.
		</p>
		</>
	);
};